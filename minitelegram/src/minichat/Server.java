package minichat;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class Server {
    private static Map<String,Connection> connectionMap = new ConcurrentHashMap<String, Connection>();
    public static void sendBroadcastMessage(Message message) {
        for (Map.Entry<String, Connection> pair: connectionMap.entrySet()) {
            try {
                pair.getValue().send(message);
            } catch (IOException e) {
                System.out.println("Сообщение не отправлено");
//                ConsoleHelper.writeMessage("Не получилось отправить сообщение для:" + pair.getValue().getRemoteSocketAddress().toString());
            }
        }
    }

    private static class Handler extends Thread {
        public Socket socket;

        public Handler(Socket socket) {
            this.socket = socket;
        }
        private String serverHandshake(Connection connection) throws IOException, ClassNotFoundException {

            while (true) {
                Message nameRequest = new Message(MessageType.NAME_REQUEST);
                connection.send(nameRequest);
                Message receivedMessage = connection.receive();

                if (!receivedMessage.getType().equals(MessageType.USER_NAME)) {

                } else if (receivedMessage.getData().isEmpty()) {

                } else if (connectionMap.containsKey(receivedMessage.getData())) {

                } else {

                    connectionMap.put(receivedMessage.getData(), connection);
                    connection.send(new Message(MessageType.NAME_ACCEPTED));
                    return receivedMessage.getData();
                }
            }
        }
        private void sendListOfUsers(Connection connection, String userName) throws IOException {
            for (Map.Entry<String, Connection> pair: connectionMap.entrySet()) {
                if (!pair.getKey().equals(userName)) {
                    connection.send(new Message(MessageType.USER_ADDED, pair.getKey()));
                }
            }
        }
        private void serverMainLoop(Connection connection, String userName) throws IOException, ClassNotFoundException {
            while (true) {
                Message receivedMessage = connection.receive();
                if (!(receivedMessage.getType()== MessageType.TEXT)) {
                    ConsoleHelper.writeMessage("Error!");
                } else {
                    sendBroadcastMessage(new Message(MessageType.TEXT,userName+": " + receivedMessage.getData()));
                }
            }
        }

        public void run() {
            ConsoleHelper.writeMessage("Соединение с удаленным сервером " + socket.getRemoteSocketAddress() + " установлено.");
            String userName = null;
            try (Connection connection = new Connection(socket)) {
                userName = serverHandshake(connection);
                sendBroadcastMessage(new Message(MessageType.USER_ADDED, userName));
                sendListOfUsers(connection,userName);
                serverMainLoop(connection,userName);
            } catch (IOException e) {
                ConsoleHelper.writeMessage("Ошибка при обмене данными с удаленным адресом!");
            if (userName != null) {
                connectionMap.remove(userName);
                sendBroadcastMessage(new Message(MessageType.USER_REMOVED,"Пользователь " + userName + " покинул чат."));
            }

            } catch (ClassNotFoundException e) {
                ConsoleHelper.writeMessage("Ошибка при обмене данными с удаленным адресом!");
                if (userName != null) {
                    connectionMap.remove(userName);
                    sendBroadcastMessage(new Message(MessageType.USER_REMOVED,"Пользователь " + userName + " покинул чат."));
                }
            }
            finally {
                if (userName != null) {
                    connectionMap.remove(userName);
                    sendBroadcastMessage(new Message(MessageType.USER_REMOVED,userName));
                }
            }
        }
    }
    public static void main(String[] args) throws IOException {
        ConsoleHelper.writeMessage("Введите порт сервера");
        int port = ConsoleHelper.readInt();
        ServerSocket ss = null;
        try {
            ss = new ServerSocket(port);
            ConsoleHelper.writeMessage("Сервер запущен");
            while (true) {
                Socket s = ss.accept();
                new Handler(s).start();
            }
        } catch (IOException e) {

            System.out.println(e.getMessage());
        } finally {
            ss.close();
        }
    }
}
