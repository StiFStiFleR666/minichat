package minichat.client;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BotClient extends Client {
    @Override
    protected SocketThread getSocketThread() {
        return new BotSocketThread();
    }

    @Override
    protected boolean shouldSendTextFromConsole() {
        return false;
    }

    @Override
    protected String getUserName() {
        int X = (int) (Math.random() * 100);
        String botName = "date_bot_" + X;
        return botName;
    }

    public class BotSocketThread extends Client.SocketThread {
        @Override
        protected void clientMainLoop() throws IOException, ClassNotFoundException {
            sendTextMessage("Привет чатику. Я бот. Понимаю команды: дата, день, месяц, год, время, час, минуты, секунды.");

            super.clientMainLoop();
        }

        @Override
        protected void processIncomingMessage(String message) {
            DateFormat formatDate;
            Calendar calendar = Calendar.getInstance();
            System.out.println(message);
            if (message != null && message.contains(":")) {
                String[] x = message.split(": ");
                String userName = x[0];
                String command = x[1];
                if (x.length == 2) {
                    switch (x[1].toUpperCase()) {
                        case "ДАТА":
                            formatDate = new SimpleDateFormat("d.MM.YYYY");
                            sendTextMessage("Информация для " + userName + ": " + formatDate.format(calendar.getTime()));
                            break;
                        case "ДЕНЬ":
                            formatDate = new SimpleDateFormat("d");
                            sendTextMessage("Информация для " + userName + ": " + formatDate.format(calendar.getTime()));
                        case "МЕСЯЦ":
                            formatDate = new SimpleDateFormat("MMMM");
                            sendTextMessage("Информация для " + userName + ": " + formatDate.format(calendar.getTime()));
                            break;
                        case "ГОД":
                            formatDate = new SimpleDateFormat("YYYY");
                            sendTextMessage("Информация для " + userName + ": " + formatDate.format(calendar.getTime()));
                            break;
                        case "ВРЕМЯ":
                            formatDate = new SimpleDateFormat("H:mm:ss");
                            sendTextMessage("Информация для " + userName + ": " + formatDate.format(calendar.getTime()));
                            break;
                        case "ЧАС":
                            formatDate = new SimpleDateFormat("H");
                            sendTextMessage("Информация для " + userName + ": " + formatDate.format(calendar.getTime()));
                            break;
                        case "МИНУТЫ":
                            formatDate = new SimpleDateFormat("m");
                            sendTextMessage("Информация для " + userName + ": " + formatDate.format(calendar.getTime()));
                            break;
                        case "СЕКУНДЫ":
                            formatDate = new SimpleDateFormat("s");
                            sendTextMessage("Информация для " + userName + ": " + formatDate.format(calendar.getTime()));
                            break;
                    }
                }
            }

        }
    }

    public static void main(String[] args) {
        BotClient botClient = new BotClient();
        botClient.run();
    }
}
