package minichat.client;


import minichat.Connection;
import minichat.ConsoleHelper;
import minichat.Message;
import minichat.MessageType;

import java.io.IOException;
import java.net.Socket;

public class Client {
    protected Connection connection;
    private volatile boolean clientConnected = false;

    protected String getServerAddress() throws IOException {
        ConsoleHelper.writeMessage("Введите адрес сервера (localhost для запуска на локальной машине):");
        return ConsoleHelper.readString();
    }

    protected  int getServerPort() throws IOException {
       ConsoleHelper.writeMessage("Введите порт сервера:");
       return ConsoleHelper.readInt();
    }

    protected String getUserName() throws IOException {
       ConsoleHelper.writeMessage("Введите свой никнейм:");
       return ConsoleHelper.readString();
    }

    protected boolean shouldSendTextFromConsole() {
        return true;
    }

    protected SocketThread getSocketThread() {
        return new SocketThread();
    }

    protected void sendTextMessage(String text) {
        try {
            connection.send(new Message(MessageType.TEXT,text));
        } catch (IOException e) {
            ConsoleHelper.writeMessage("Error");
            clientConnected = false;
        }
    }
    public void run() {
        SocketThread x = getSocketThread();
        x.setDaemon(true);
        x.start();
        try {
            synchronized (this) {this.wait();}
        } catch (InterruptedException e) {
           ConsoleHelper.writeMessage("Ошибка");
        }
        if (clientConnected) {
            ConsoleHelper.writeMessage("Соединение установлено. Для выхода наберите команду 'exit'.");
            while (clientConnected) {
                String text = ConsoleHelper.readString();
                if(text.equalsIgnoreCase("exit")) {
                    break;
                } else {
                    if (shouldSendTextFromConsole()) {
                        sendTextMessage(text);
                    }
                }
            }
        } else {
            ConsoleHelper.writeMessage("Произошла ошибка во время работы клиента.");
        }
    }
    public class SocketThread extends Thread {
        protected void processIncomingMessage(String message) {
            ConsoleHelper.writeMessage(message);
        }
        protected void informAboutAddingNewUser(String userName) {
            ConsoleHelper.writeMessage("Участник с именем " + userName + " присоеденился к чату");
        }
        protected void informAboutDeletingNewUser(String userName) {
            ConsoleHelper.writeMessage("Участник с именем " + userName + " покинул чат");
        }
        protected void notifyConnectionStatusChanged(boolean clientCinnected) {
            clientConnected = clientCinnected;
            synchronized (Client.this) {
                Client.this.notify();
            }
        }
        protected void clientHandshake() throws IOException, ClassNotFoundException {
            while (true) {
                Message message = connection.receive();
                if(message.getType() == MessageType.NAME_REQUEST) {
                    String userName = getUserName();
                    Message nameMessage = new Message(MessageType.USER_NAME, userName);
                    connection.send(nameMessage);
                }
                else if (message.getType() == MessageType.NAME_ACCEPTED) {
                    notifyConnectionStatusChanged(true);
                    break;
                }
                else {
                    throw new IOException("Unexpected MessageType");
                }
            }
        }

        protected void clientMainLoop() throws IOException, ClassNotFoundException {
            while (true) {
                Message receivedMessage = connection.receive();
                if (receivedMessage.getType() == MessageType.TEXT) {
                    processIncomingMessage(receivedMessage.getData());
                }
                else if (receivedMessage.getType() == MessageType.USER_ADDED) {
                    informAboutAddingNewUser(receivedMessage.getData());
                }
                else if (receivedMessage.getType() == MessageType.USER_REMOVED) {
                    informAboutDeletingNewUser(receivedMessage.getData());
                } else {
                    throw new IOException("Unexpected MessageType");
                }
            }
        }

        public void run() {
            try {

                connection = new Connection(new Socket(getServerAddress(),getServerPort()));
                clientHandshake();
                clientMainLoop();
            } catch (IOException e) {
                notifyConnectionStatusChanged(false);
            } catch (ClassNotFoundException e) {
                notifyConnectionStatusChanged(false);
            }
        }
    }

    public static void main(String[] args) {
        Client client = new Client();
        client.run();
    }
}
